TextClass = EntityClass.extend({
	zindex: 4,
    text: "",
	font: "15px Verdana",
	has_stroke: false,
	color: "#ffffff",

    init: function (x, y, settings)  {
        this.parent(x, y, settings);
        
		if(settings.resize) this.resize = settings.resize;

		this.pos = { x: x,  y: y };
		if(settings.zindex) this.zindex = settings.zindex;
		if(settings.font) this.font = settings.font;
		this.has_stroke = settings.has_stroke;
		this.text = settings.text;
		if(settings.color) this.color = settings.color;
    },
	update:function()
	{
	},

	draw:function()
	{
		if(this.image!=null)
		{
			this.drawResized(this.image,this.pos.x-this.size.x/2,this.pos.y-this.size.y/2,this.resize);
		}
		
		ctx.font = this.font;
		

		if(this.has_stroke)
		{
			ctx.strokeStyle = this.color;
			ctx.strokeText(this.text,this.pos.x,this.pos.y);
		}
		else
		{
			ctx.fillStyle = this.color;
			ctx.fillText(this.text,this.pos.x,this.pos.y);
		}

	},
});

gGameEngine.factory['Text'] = TextClass;