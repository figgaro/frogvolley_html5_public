NormalBackClass = EntityClass.extend({
    physBody: null,
    _killed: false,
	zindex: 1,
    image:null,

    init: function (x, y, settings)  {
        this.parent(0, 0, settings);
        
        var startPos = {
            x: 0,
            y: 0
        };

		
		if(settings.image)
		{
			this.image = settings.image;
			this.size.x = this.image.width;
			this.size.y = this.image.height;
		}

		if(settings.zindex) this.zindex = settings.zindex;
    },

    //-----------------------------------------
    kill: function () {
        // Destroy me as an entity
        this._killed = true;
    },

	update:function()
	{
		
	},

	draw:function()
	{
		ctx.drawImage(this.image,this.pos.x,this.pos.y);
	},


});

gGameEngine.factory['NormalBack'] = NormalBackClass;