

// These are global shorthands we declare for Box2D primitives
// we'll be using very frequently.
Vec2 = Box2D.Common.Math.b2Vec2;
BodyDef = Box2D.Dynamics.b2BodyDef;
Body = Box2D.Dynamics.b2Body;
FixtureDef = Box2D.Dynamics.b2FixtureDef;
Fixture = Box2D.Dynamics.b2Fixture;
World = Box2D.Dynamics.b2World;
MassData = Box2D.Collision.Shapes.b2MassData;
PolygonShape = Box2D.Collision.Shapes.b2PolygonShape;
CircleShape = Box2D.Collision.Shapes.b2CircleShape;
DebugDraw = Box2D.Dynamics.b2DebugDraw;
RevoluteJointDef = Box2D.Dynamics.Joints.b2RevoluteJointDef;

PhysicsEngineClass = Class.extend({
    world: null,

    PHYSICS_LOOP_HZ : 1.0 / 60.0,

    //-----------------------------------------
    create: function () {
        gPhysicsEngine.world = new World(
            new Vec2(0, 0.22), // Gravity vector
            false           // Don't allow sleep
        );
    },

    //-----------------------------------------
    update: function () {
        var start = Date.now();

        gPhysicsEngine.world.Step(
            gPhysicsEngine.PHYSICS_LOOP_HZ,    //frame-rate
            10,                 //velocity iterations
            10                  //position iterations
        );
        gPhysicsEngine.world.ClearForces();

        return(Date.now() - start);
    },

    //-----------------------------------------
    addContactListener: function (callbacks) {
        var listener = new Box2D.Dynamics.b2ContactListener();

        if(callbacks.PostSolve) listener.PostSolve = function (contact, impulse) {
            callbacks.PostSolve(contact.GetFixtureA().GetBody(),
                                contact.GetFixtureB().GetBody(),
                                impulse.normalImpulses[0]);
        };

        gPhysicsEngine.world.SetContactListener(listener);
    },

    //-----------------------------------------
    registerBody: function (bodyDef) {
        var body = gPhysicsEngine.world.CreateBody(bodyDef);
        return body;
    },

    //-----------------------------------------
    addBody: function (entityDef) {
        var bodyDef = new BodyDef();

        var id = entityDef.id;

        if(entityDef.type == 'static') {
            bodyDef.type = Body.b2_staticBody;
        } else {
            bodyDef.type = Body.b2_dynamicBody;
        }

        bodyDef.position.x = entityDef.x;
        bodyDef.position.y = entityDef.y;

        if(entityDef.userData)  bodyDef.userData = entityDef.userData;

		bodyDef.allowSleep = true;
		bodyDef.damping = entityDef.damping;

        var body = this.registerBody(bodyDef);
        var fixtureDefinition = new FixtureDef();

        if(entityDef.useBouncyFixture) {
            fixtureDefinition.density = 0.001;
            fixtureDefinition.restitution = 0.65;
        }

		fixtureDefinition.friction = 0.1;

        // Now we define the shape of this object as a box
		if(entityDef.shape=="circle")
		{
			fixtureDefinition.friction = 0.1;
			fixtureDefinition.shape = new CircleShape();
			fixtureDefinition.shape.m_radius = entityDef.halfWidth;
		}
		else if(entityDef.shape=="polygon")
		{
			fixtureDefinition.friction = 0.5;
			fixtureDefinition.shape = new PolygonShape();
			fixtureDefinition.shape.SetAsArray(entityDef.vertex);
			fixtureDefinition.density = 100.0;
			fixtureDefinition.mass = 130.0;
		}
		else if(entityDef.shape=="net")
		{
			fixtureDefinition.friction = 0.0;
			fixtureDefinition.shape = new PolygonShape();
			fixtureDefinition.shape.SetAsBox(entityDef.halfWidth, entityDef.halfHeight);
		}
		else if(entityDef.shape=="wall")
		{
			fixtureDefinition.friction = 0.0;
			fixtureDefinition.shape = new PolygonShape();
			fixtureDefinition.shape.SetAsBox(entityDef.halfWidth, entityDef.halfHeight);
		}
		else
		{
			fixtureDefinition.friction = 0.2;
			fixtureDefinition.shape = new PolygonShape();
			fixtureDefinition.shape.SetAsBox(entityDef.halfWidth, entityDef.halfHeight);

			fixtureDefinition.density = 10.0;
			fixtureDefinition.mass = 30.0;

		}
        body.CreateFixture(fixtureDefinition);
		
		var massdata = { mass:(entityDef.mass)?entityDef.mass:100, center: new Vec2(0,0) };
		body.SetMassData(massdata);

        return body;
    },

    //-----------------------------------------
    removeBody: function (obj) {
        gPhysicsEngine.world.DestroyBody(obj);
    }

});

var gPhysicsEngine = new PhysicsEngineClass();

