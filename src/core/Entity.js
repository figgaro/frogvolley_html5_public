
EntityClass = Class.extend({
    // can all be referenced by child classes
    pos : {x:0,y:0},
    size : {x:0,y:0},
    last : {x:0,y:0},
	speed : new Vec2(0,0),
	rotation : 0,
	rotation_speed:0,
	resize: 1,
	zindex: 2,
    image:null,
	frames:[],
	frame_set:0,
	frame_cont:0,
	MAX_ROTATION:0.2,

    init : function() { },
    // can be overloaded by child classes
    update : function() { 
		
	},

	drawRotatedImage: function(image, x, y, angle) { 
 
		// save the current co-ordinate system 
		// before we screw with it
		ctx.save(); 
	 
		// move to the middle of where we want to draw our image
		ctx.translate(x, y);
	 
		// rotate around that point, converting our 
		// angle from degrees to radians 
		ctx.rotate(angle);
	 
		// draw it up and to the left by half the width
		// and height of the image 
		ctx.drawImage(image, -(image.width/2), -(image.height/2));
	 
		// and restore the co-ords to how they were when we began
		ctx.restore(); 
	},

	
	drawResized: function(image, x, y, per) 
	{
		ctx.drawImage(image, 0,0, image.width, image.height, x, y, image.width*per, image.height*per);
	},
	
	drawFrameResized: function(image, x, y, frame) 
	{
		ctx.drawImage(image, frame.x,frame.y, frame.w, frame.h, x-frame.w/2, y-frame.h/2, frame.w*this.resize, frame.h*this.resize);
	},
	

	loadImage: function(settings, x, y)
	{
		var startPos = {x:x,y:y};

		if(settings.image)
		{
			this.image = settings.image;

			if(settings.sprite != null && (settings.sprite.x>1 || settings.sprite.y>1) )
			{
				var framex = this.image.width/settings.sprite.x;
				var framey = this.image.height/settings.sprite.y;
				
				var cont = 0;
				for(var i=0;i<settings.sprite.y*settings.sprite.x;i++)
				{
					for(var j=0;j<settings.sprite.x;j++)
					{
						this.frames[cont] = {};
						this.frames[cont].x = j*framex;
						this.frames[cont].y = i*framey;
						this.frames[cont].w = framex;
						this.frames[cont].h = framey;

						cont++;
					}
				}

				//startPos.x += this.image.width/2;
				//startPos.y += this.image.height/2;
			}

			else
			{
				this.size.x = this.image.width;
				this.size.y = this.image.height;
			}
		}

		return startPos;
	},

	setPosition: function(x,y)
	{
		this.pos.x = x;
		this.pos.y = y;

		if(this.physBody)
		{
			this.physBody.SetPosition( new Vec2(x/1000, y/1000) );
		}
	},
	
	setSpeed: function(x,y)
	{
		this.speed.x = x;
		this.speed.y = y;

		if(this.physBody)
		{
			var sp = new Vec2(this.speed.x/1000,this.speed.y/1000);
			this.physBody.SetLinearVelocity( sp );
		}
	},

	addSpeed: function(x,y)
	{
		this.speed.x += x;
		this.speed.y += y;

		if(this.physBody)
		{
			var sp = new Vec2(this.speed.x/1000,this.speed.y/1000);
			this.physBody.SetLinearVelocity( sp );
		}
	},
	
	addRotation:function(r)
	{
		this.rotation_speed += r;

		if(this.rotation_speed > this.MAX_ROTATION) this.rotation_speed = this.MAX_ROTATION;
		if(this.rotation_speed < -this.MAX_ROTATION) this.rotation_speed = -this.MAX_ROTATION;
	},

	setRotation:function(r)
	{
		this.rotation_speed = r;

		if(this.rotation_speed > this.MAX_ROTATION) this.rotation_speed = this.MAX_ROTATION;
		if(this.rotation_speed < -this.MAX_ROTATION) this.rotation_speed = -this.MAX_ROTATION;
	},

});


AnimationClass = Class.extend({
    // can all be referenced by child classes
    startFrame: 0,
	endFrame: 0,
	currentFrame: 0,
	loop:false,
	ended: false,
	type: "",
		
	frames_per_animation: 60/2.5,
	frame_count:0,
	
	keepLastFrame:false,
	
	
	init: function(s,e,l,t)
	{
		this.startFrame = s;
		this.endFrame = e;
		this.loop = l;
		this.type = t;

		this.currentFrame = s;
	},

	update: function()
	{
		this.frame_count++;
		if(this.frame_count%this.frames_per_animation!=0) return false;

		if(this.keepLastFrame && this.currentFrame==this.endFrame) return false;

		this.currentFrame++;
		if(this.currentFrame>this.endFrame)
		{
			if(this.loop) this.currentFrame = this.startFrame;
			else			this.ended = true;
		}

		return this.ended;
	},
	
	keepLast: function(kl)
	{
		this.keepLastFrame = kl;
	},

});

