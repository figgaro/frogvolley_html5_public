var canvas = null;
var ctx = null;
var game_mode = false;
var updateintervalID = null;
var game_ratio = 320/480;
var game_scale = 1;

var assets = {	'menu2' : 'assets/Textures/menu2.png',
				'start_normal' : 'assets/Textures/start_normal.png',
				'about_normal' : 'assets/Textures/about_normal.png',
				'start_focused' : 'assets/Textures/start_focused.png',
				'start_pressed' : 'assets/Textures/start_pressed.png',
				'about_focused' : 'assets/Textures/about_focused.png',
				'about_pressed' : 'assets/Textures/about_pressed.png',
				'powdercode_logo' : 'assets/Textures/powdercode_logo.png',
				'llosa' : 'assets/Textures/llosa.png',
				'menu' : 'assets/Textures/menu.png',
				'boto_normal' : 'assets/Textures/boto_normal.png',
				'boto_focused' : 'assets/Textures/boto_focused.png',
				'boto_pressed' : 'assets/Textures/boto_pressed.png',
				'back' : 'assets/Textures/back.png',
				'ball' : 'assets/Textures/ball.png',
				'end' : 'assets/Textures/end.png',
				'frog' : 'assets/Textures/frog.png',
				'frogvs' : 'assets/Textures/frogvs.png',
				'menu_info' : 'assets/Textures/menu_info.png',
				'net_picture' : 'assets/Textures/net_picture.png',
				'pause_menu' : 'assets/Textures/pause_menu.png',
				'point' : 'assets/Textures/point.png',
				'tronc' : 'assets/Textures/tronc.png',
				'sound' : 'assets/Textures/sound.png'
			};

var sounds = [	'assets/Music/boing.mp3',
				'assets/Music/buzzer.mp3',
				'assets/Music/hit.mp3',
				'assets/Music/net.mp3',
				'assets/Music/yeah.mp3',
				'assets/Music/back_music.mp3',
			];

var assets_number = 24;

var frames = [];
var current = 0;
var loaded = 0;

var onImageLoad = function(){
    loaded++;

    if(loaded>=assets_number)
	{ 		
		$("#loadingArea").hide();
		$("#menuArea").show();
		gGameEngine.setup();

		startNextSound();
	}
	else $("#progressPanel").css('width',(loaded/assets_number)*100+'%');
};

var setup = function() {
    body = document.getElementById('body');
    canvas = document.getElementById('my_canvas');

    ctx = canvas.getContext('2d');

    frames = new Array();
	var i = 0;
    for(var ass in assets)
    {
		var src = assets[ass];

        frames[i] = new Image();
        frames[i].onload = onImageLoad;
        frames[i].src = src;
		gSimpleImage[ass] = frames[i];

		i++;
    }

	canvas.width = 960;
    canvas.height = 640;
	resizeGame();

	window.onresize = function(event) { resizeGame();};
	canvas.addEventListener("touchstart", canvasTouchEvent, false);
	canvas.addEventListener("touchmove", canvasTouchEvent, false);
	canvas.addEventListener("click", canvasTouchEvent, false);
};





function showMenu(num)
{
	$("#gameMenu").hide();
	$("#mainMenu").hide();
	$("#aboutMenu").hide();

	if(num==1) $("#mainMenu").show();
	if(num==2) $("#gameMenu").show();
	if(num==3) $("#aboutMenu").show();
}

function startGame(is_versus)
{
	game_mode = is_versus;
	$("#menuArea").hide();
	$("#gameArea").show();
	gGameEngine.start();

	ga('send', 'StartGame');
}

function endGame()
{
	ga('send', 'EndGame: '+gGameEngine.PUNTS_1+'-'+gGameEngine.PUNTS_2);

	document.body.removeChild( gGameEngine.statsdoob.domElement );
	clearInterval(updateintervalID);
	gGameEngine.pause_menu.is_show = false;
	gGameEngine.PAUSE = false;
	gGameEngine.restartGame();
	gSM.stopAll();

	showMenu(1);
	$("#menuArea").show();
	$("#gameArea").hide();
}

function resizeGame()
{
	var scalificator = 0.75;
	if(isMobile.any())
	{
		scalificator = 1;
		$(".gradient").hide();
	}

	ctx.scale(1/game_scale,1/game_scale);

	var width = window.innerWidth;
	var height = window.innerHeight;
	var cw = 960;
	var ch = 640;

	//see bigger ratio
	var screen_ratio =  height/width;
	var new_game_scale = 1;

	if(game_ratio>screen_ratio){
		ch = height*scalificator;
		if(ch>640) ch = 640;
		cw = ch/game_ratio;
		new_game_scale = cw/480;
	}
	else{
		cw = width*scalificator;
		if(cw>960) cw = 960;
		ch = cw*game_ratio;
		new_game_scale = cw/480;
	}

	ctx.scale(new_game_scale,new_game_scale);
	$("#gameArea").width(cw);
	$("#gameArea").height(ch);

	$("#loadingArea").width(cw);
	$("#loadingArea").height(ch);

	
	//$("#menuArea").width(cw);
	//$("#menuArea").height(ch);
	scaleShowArea("#menuArea",cw/960);

	game_scale = new_game_scale;
}


function scaleShowArea(menu,news)
{
	$("#menuArea").css('transform','translate('+getHalfLeft(news)+') scale('+news+','+news+')');
	$("#menuArea").css('-ms-transform','translate('+getHalfLeft(news)+') scale('+news+','+news+')');
	$("#menuArea").css('-webkit-transform','translate('+getHalfLeft(news)+') scale('+news+','+news+')');
	$("#menuArea").css('-o-transform','translate('+getHalfLeft(news)+') scale('+news+','+news+')');
	$("#menuArea").css('-moz-transform','translate('+getHalfLeft(news)+') scale('+news+','+news+')');

	$("#infoArea").css('top',$("#menuArea").height()*news+50);
	//$("#menuArea").css('left',((window.innerWidth-960*(1-news))/2)+"px");
}

function getHalfLeft(val)
{
	var per = -(1-val)*50;
	var perx =  -(1-val)*960/2 + (window.innerWidth-960*val)/2;
	return perx+"px,"+per+"%";
}


var actual_sound = 0;


function startNextSound()
{
	if(actual_sound<sounds.length) gSM.loadAsync(sounds[actual_sound], startNextSound);
	actual_sound++;
}


function canvasTouchEvent(event)
{
	if(event.touches==null) 
	{
		var click = {pageX:event.x,pageY:event.y};
		event.touches = [];
		event.touches.push(click);
	}
	

	for (var i=0; i<event.touches.length; i++)
	{
		var touch = event.touches[i];
		var x = (touch.pageX-$('#my_canvas').offset().left)/game_scale;
		var y = (touch.pageY-$('#my_canvas').offset().top)/game_scale;

		if(x<35 && y<35){
			gGameEngine.pause_menu.is_show = true;
			gGameEngine.PAUSE = true;
		}

		if(x>445 && y<35){
			gSM.togglemute();
		}

		if( gGameEngine.PAUSE && x>195 && x<285 && y>136 && y<211)
		{
			if(y>171) {
				endGame();
			}
			else {
				gGameEngine.pause_menu.is_show = false;
				gGameEngine.PAUSE = false;
			}
		}

		if(!gGameEngine.PAUSE)
		{		
			//game_mode 2 players
			if(game_mode)
			{
				//frog1
				if(x<240){
					if(y<200)	gGameEngine.frog1.jump();
					gGameEngine.frog1.moveTo(x);
				}
				//frog2
				else {
					if(y<200)	gGameEngine.frog2.jump();
					gGameEngine.frog2.moveTo(x);
				}
			}
			else //game_mode 1 player
			{
				//move
				if(x<240) gGameEngine.frog1.moveTo(x);
				//jump
				else gGameEngine.frog1.jump();
			}
		}
	}
}



var isMobile = {
    Android: function() {
        return navigator.userAgent.match(/Android/i);
    },
    BlackBerry: function() {
        return navigator.userAgent.match(/BlackBerry/i);
    },
    iOS: function() {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
    },
    Opera: function() {
        return navigator.userAgent.match(/Opera Mini/i);
    },
    Windows: function() {
        return navigator.userAgent.match(/IEMobile/i);
    },
    any: function() {
        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
    }
};